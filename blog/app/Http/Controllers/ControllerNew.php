<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Message;

class ControllerNew extends Controller
{

   public function main()
   {
    return view('layouts.main');
   }

    public function messageFromDataBase()
    {
        $msg = Message::paginate(5);
        $head_table = [
            '№',
            'name',
            'email',
            'message',
            'image'
        ];
        $number = ($msg->currentPage()-1)*$msg->perPage() + 1;
        return view('layouts.msgDb',[
            'msg' => $msg,
            'headLine' => 'message from database',
            'head_table' => $head_table,
            'number' => $number
        ]);
    }


    public function EditData(Message $msg)
    {
        $img = $msg->images;
        return view('layouts.edit',[
            'msg' => $msg,
            'img' => $img,
            'headLine' => 'edit data'
        ]);
    }
 
}
