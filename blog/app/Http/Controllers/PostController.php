<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Message;
use App\Images;
class PostController extends Controller
{
 
    public function index()
    {
        //
    }


    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'message'=> 'required'
        ]);

        Message::create([
            'name' => request('name'),
            'email'=> request('email'),
            'message'=> request ('message')
        ]);
        
        return redirect()->route('main');
        
    }

    public function updateData(Request $request, Message $msg)
    {
        $request->validate([
            'name' => 'required',
            'email' => 'required',
            'message'=> 'required'
        ]);
        $msg->update([
            'name' => $request['name'],
            'email' => $request['email'],
            'message' => $request['message']
        ]);
        if(!$request['save_img'])
        {
            Images::where('message_id','=',$msg->id)->delete();
            if($request->hasFile('image')) 
            {
                $file = $request->file('image');
                foreach($file as $f)
                {
                    $dir ="/img_db/".$f->getClientOriginalName();
                    $img = Images::where('directory','=',$dir)->get();
                    if(count($img))
                    {
                        $rand = rand(1,PHP_INT_MAX)/100000;
                        $fileName = $rand.$f->getClientOriginalName();
                        $f->move(public_path().'/img_db',$fileName);
                    }
                    else 
                    {
                        $fileName = $f->getClientOriginalName();
                        $f->move(public_path().'/img_db', $fileName);
                    }
                    $dir = "/img_db/".$fileName;
                    Images::create([
                        'message_id' => $msg->id,
                        'directory' => $dir
                    ]);
                }
            }
         }
        return redirect()->route('msg');
    }


   
    public function show($id)
    {
        //
    }

   
    public function edit($id)
    {
        //
    }

  
    public function update(Request $request, $id)
    {
        //
    }


    public function destroy($id)
    {
        //
    }
}
