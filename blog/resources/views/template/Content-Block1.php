<div class="container-fluid cont-fl">
	<div class="row">
    	<div class="col-md-5">
        	<h1 class="h-text"> FIRST CLASS <br>LOGISTICS</h1>
			<div class="pull-left">
				<a href="https://www.youtube.com/watch?time_continue=1&v=1zclkA9PkFQ">
				 	<img src="/img/play.jpg">
				</a>
			</div>
			<div class="pull-left m-l-20">
			 	<span class="content-text">
					WATCH VIDEO
				</span>
			 	<br>
			 	<span class="content-text-again">
					FAST AND SECURE DELIVERY
				</span>
			</div>
        </div>
		<div class="col-md-7 bkGr m-p-l-0">
        	<div class="track-id">
				<br><br><br>
				<img src="/img/logo1.jpg">
				<br>
				<h3 class="track-order-text"><strong> TRACK YOUR ORDER </strong></h3>
				<p class="track-order-text"> ENTER YOUR TRACK ID FOR INSTANT SEARCH</p>
				<input class="input-id" type="text" name="track_id" placeholder="TRACK ID">
			</div>
        </div>
    </div>
</div>
<div class="container">
    <div class"row">
        <div class="col-md-4 pull-right">
        	<a href=#><img src="/img/social.jpg"></a>
        </div>
    </div>
</div>

</div>
</div>
</div>