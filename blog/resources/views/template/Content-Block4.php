<div class="banner-2">
	<div class="back-ground-color">
		<div class="container">
			<div class="row">
				<div class="col-md-12 text-center">
					<h1 class="heading text-white"> TESTIMONIALS</h1>
					<div class="blue-line"></div>
					<p class="mw-800 ">
						Shipping Logistics is one of the top logistics providers in the industry. and first class customer service and live by the 'Never Say Never' attitude to accomplish every mission.
					</p>
					<div class="col-md-3 m-0-auto float-none">
						<div class="pull-left">
							<img src="/img/avatar-1.jpg" class="img-circle avatar-img">
						</div>
						<div class="pull-left">
							<p class="avatar-name"> Kate Douglas </p>
							<p class="company">Company Co. In. </p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>