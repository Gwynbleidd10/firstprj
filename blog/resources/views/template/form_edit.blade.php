<form enctype="multipart/form-data" action="{{route('edit_msg',['id'=>$msg['id']])}}" method="POST">
    {{csrf_field()}}
    <div class="col-md-6">
        <input type="text" class ="form-control input-estimate" value="{{$msg['name']}}" name="name">
    </div>
    <div class="col-md-6">
        <input type="text" class ="form-control input-estimate" value="{{$msg['email']}}" name="email">
    </div>
    <div class="col-md-12">
        <textarea type="text" class="form-control input-estimate h-150" name="message">
         {{$msg['message']}}
        </textarea>
    </div>
    <div class="col-md-12">
        <h4>Current image:</h4> 
        @if(count($img))
            @foreach($img as $im)
            <img src="{{ $im['directory'] }}" class='img-responsive m-0-auto' height=240px width=240px>
            @endforeach
        @else 
            Image not exist <br>
        @endif
        <label>
            <input type="checkbox" name="save_img">
            Save current
        </label>
    </div>
    <div class="col-md-12">
        <input type="file" name="image[]" accept="image/*" multiple="multiple">
    </div>
    <div class="col-md-12">
        <button type="submit" class="button">EDIT RECORD</button>
    </div>
</form>