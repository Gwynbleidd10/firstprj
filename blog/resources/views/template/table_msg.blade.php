{{ $msg->links() }}                  
<table class="table table-bordered text-center">  
  <tr>   
    @foreach($head_table as $head)
      <td>
        <strong>
          {{ucfirst($head)}}
        </strong>
      </td> 
    @endforeach
  </tr>
  @foreach($msg as $message)
    <tr>
      <td>
        {{$number++}}
      </td>
      @for ($i=1;$i<count($head_table)-1;$i++)
        <td class = "text-left">
          {{$message[$head_table[$i]]}}
        </td>
      @endfor
      <td>
        @if(count($message->images))   
          @foreach($message->images as $img)
            <img src={{$img->directory}} class='img-responsive'>
          @endforeach
        @else 
          Image not exist
        @endif
      </td>                    
      <td class="table-button">
        <form action={{route('edit_post',['msg'=>$message['id']])}} method=POST>
          {{ csrf_field() }}
          <button type="submit" class = "btn btn-success">
            Редактировать запись
          </button>
        </form>
      </td>
    </tr>
  @endforeach        
</table>

<br><br>
</div>
</div>
</div>
</div>
</div>
