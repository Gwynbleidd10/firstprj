<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/main','ControllerNew@main')->name('main');
Route::post('/posts','PostController@store')->name('post_store');
Route::get('/msgDb','ControllerNew@messageFromDataBase')->name('msg');
Route::get('/edit/{msg}','ControllerNew@EditData')->name('edit_get');//*
Route::post('/edit/{msg}','ControllerNew@EditData')->name('edit_post');
Route::post('/editMsg/{msg}','PostController@updateData')->name('edit_msg');
