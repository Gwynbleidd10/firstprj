<?php
require 'flight-master/flight/Flight.php';
require_once('ConnectionDB.php'); 
require 'layout.php';
Flight::route('/index/main',array('Layout_page','layout_main'));

Flight::route('/index/@page',array('Layout_page','layout_default'));

Flight::route('/index/edit/@id',array('Layout_page','layout_edit'));

Flight::route('/msgDb(/@page)', array('Layout_page','layout_msgDb'));

Flight::start();
?>
