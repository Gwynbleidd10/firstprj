<?php


class Layout_page
{
    public static function layout_main()
    {
        Flight::render('./layout/main');
    }

    public static function layout_default($layout)
    {
        Flight::render('./layout/'.$layout);
        Flight::render('./layout/main');
    }

    public static function layout_edit($id)
    {  
        Flight::render('./layout/edit',array('id'=>$id));
    }

    public static function layout_msgDb($page)
    {
        if($page==null) $page=1;
        Flight::render('./layout/msgDb',array('active_page'=>$page));
    }

}

?>