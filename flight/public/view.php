<?php


class View {

    static protected $layouts = [
        'main' => [
            'navigation' => 'regions/navigation'
        ],
        'msgDb'=>[
            'navigation' => 'regions/navigation'
        ],
        'edit'=>[
            'navigation' => 'regions/navigation'
        ]
    ];

    public static function renderTemplate($layout, $context) {
        ob_start();
        include('views/'.self::$layouts[$layout]['navigation'].'.php');
        $data = ob_get_contents();
        ob_end_clean();
        return $data;
    }

    public static function renderLayout($layoutName, $regions) {
        include('views/layout/'.$layoutName.'.php');   
    }
}
?>