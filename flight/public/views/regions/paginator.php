<?php
$count_str = OperationDB::SelectQuery("SELECT COUNT(1) FROM Message");
  $count_record = 5;
  $row = $count_str->fetch(PDO::FETCH_LAZY);
  $count_pages= ceil($row[0]/$count_record);
  $count_show_page = 5;//*
  $url_page="/msgDb/";
  $offset= $count_record*($active_page-1);

  // Find start and end page for output
  if($count_pages > $count_show_page && $active_page>=3) {
      if ($active_page <= 3) $start = 1;
      else $start = $active_page - 2;
      if ($active_page + 2 >= $count_pages) $end = $count_pages;
      else $end = $active_page + 2;
      while ($end - $start +1 != $count_show_page) {$start--;}
  }
  else
  {
      $start = 1;
      if($count_pages > $count_show_page)
          $end = $count_show_page;
      else $end = $count_pages;
  }

  echo "<ul class='pagination'>";
  if( $start != 1) {

      echo "<li> <a href=" . $url_page . ($active_page - 3) . "> ... </a></li>";
  }

  if($count_show_page > $count_pages ) $count_show_page = $count_pages;

  for ($i = $start ; $i <= $end; $i++)
  {
      if($i == $active_page)
          echo "<li class='active'><a href = ".$url_page . $start.">$start</a></li>";
      else echo "<li><a href = ".$url_page . $start.">$start</a></li>";
      $start++;
  }

  if($end != $count_pages) echo "<li><a href= ".$url_page.($active_page+3).">...</a></li>";
  echo "</ul>";

  ?>