<?php


class Route {

    protected static $paths = [
        'msg' => [
            'name' => 'MsgController',
            'method' => 'index',
            'layout'=> 'main'
        ],
        'msgDb' => [
            'name' => 'MsgController',
            'method' => 'index',
            'layout' => 'msgDb'
        ],
        'edit'=>[
            'name' => 'MsgController',
            'method' => 'index',
            'layout' => 'edit'
        ]
    ];

    public static function getController($path) {
        require_once(self::$paths[$path]['name'].'.php');

        return  new self::$paths[$path]['name']();
    }

    public static function getMethod($path) {
        return self::$paths[$path]['method'];
    }

    public static function getLayout($path)
    {
       return self::$paths[$path]['layout'];
    }



}

?>